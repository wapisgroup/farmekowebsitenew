<div class="row">
    <div class="col-sm-24">
        <h1>Diplomovaný farmaceutický asistent</h1>

        <p><img style="" class="" title="Farmacie" alt="Farmacie" src="http://farmeko.cz/uploaded/nove_obrazky_menu/leky.jpg" rel="" align="right" vspace="0" hspace="-1">Atraktivní tříletý vzdělávací program <strong>Diplomovaný farmaceutický asistent (53-43-N/1.). akreditovaný MŠMT a MZ ČR </strong>je určen pro absolventy s úplným středním vzděláním zakončeným maturitní zkouškou.<br><br>Výuka je realizována na základě vzdělávacího programu (VP):
            <a title="Diplomovaný farmaceutický asistent - vzdělávací program" href="http://farmeko.cz/uploaded/Dokumenty/8-b%29-vzdelavaci-program-dipl.far.asistent.pdf" target="_self">Diplomovaný farmaceutický asistent</a>, a to v&nbsp;denní i kombinované (dálkové) formě vzdělávání. Školní rok je rozdělen na období zimní a letní, každé období je zakončeno obdobím k&nbsp;získání hodnocení (zkouškovým obdobím). Absolventi získávají <strong>titul DiS. (diplomovaný specialista).</strong>
        </p>
        <ul>
            <li><a title="Diplomovaný farmaceutický asistent - denní forma" href="http://farmeko.cz/stranka/diplomovany-farmaceuticky-asistent-denni-forma/6" target="_self"><strong>Diplomovaný farmaceutický asistent - </strong><strong>denní forma</strong></a></li>
            <li><a title="Diplomovaný farmaceutický asistent - kombinovaná forma" href="http://farmeko.cz/stranka/diplomovany-farmaceuticky-asistent-kombinovana-forma/7" target="_self"><strong>Diplomovaný farmaceutický asistent - </strong><strong>kombinovaná forma</strong></a></li>
        </ul>
        <p>Obor vzdělání je zaměřen především na výuku medicínských a farmaceutických disciplín pod vedením zkušených pedagogů – odborníků z praxe (farmaceuti, lékaři). Výuka probíhá v moderně vybavených učebnách, laboratořích, učebnách ICT. Významnou součástí výuky je odborná praxe v lékárnách.</p>
        <p>Vzdělávací program připravuje studenty k&nbsp;výkonu povolání farmaceutického asistenta, který je způsobilý poskytovat zdravotní péči v&nbsp;rozsahu působnosti stanovené zákonem č. 105/2011 Sb., o podmínkách získávání a uznávání způsobilosti k&nbsp;výkonu nelékařských zdravotnických povolání a k&nbsp;výkonu činností souvisejících s&nbsp;poskytováním zdravotní péče a&nbsp;o&nbsp;změně některých souvisejících zákonů (zákon o nelékařských zdravotnických povoláních).</p>
        <p>Vzdělávání se zaměřuje na osvojení vědomostí a profesních dovedností nezbytných pro plánování, poskytování, vyhodnocování a&nbsp;zajišťování zdravotní (lékárenské) péče a na vytváření žádoucích profesních postojů, návyků a dalších osobnostních kvalit zdravotnického pracovníka. </p>

        <h3>Uplatnění absolventa v praxi</h3>
        <p>Absolvent vzdělávacího programu diplomovaný farmaceutický asistent najde uplatnění na všech farmaceutických pracovištích, zejména ve všech typech lékáren, v&nbsp;laboratořích pro kontrolu léčiv, výdejnách zdravotnických prostředků, pracovištích zabývajících se výrobou a distribucí léčiv, prodejnách léčivých rostlin a při dalších činnostech, kde se zachází s&nbsp;léčivy (ve zdravotnických zařízeních ambulantní a lůžkové péče), jako reprezentant farmaceutických firem apod.<br><br>&nbsp;Absolvent se může dále vzdělávat v&nbsp;rámci dalšího vzdělávání zdravotnických pracovníků nebo studiem na vysoké škole.&nbsp;</p>
        <p><strong>Moduly (předměty) učebního plánu:</strong> cizí jazyk, latinský jazyk, chemie a biochemie, psychologie a komunikace, informační a komunikační technologie, veřejné zdravotnictví, výchova ke zdraví, první pomoc a medicína katastrof, anatomie a fyziologie, patofyziologie a patologie, mikrobiologie a hygiena, výživa člověka, farmaceutická botanika, analýza léčiv, farmakologie, příprava léčiv, laboratorní technika, farmakognózie, chemie léčiv, výdejní činnost, zdravotnické prostředky, základy radiologie, lékárenství, kosmetologie, seminář k absolventské práci, odborná praxe.</p>
        <ul>
            <li>Studium je zakončeno absolutoriem, absolventi získávají vysvědčení o absolutoriu a diplom absolventa vyšší odborné školy.</li>
            <li>Absolventi jsou oprávněni používat titul diplomovaný specialista - DiS.</li>
            <li>Absolutorium studenti skládají z těchto předmětů: farmakologie, příprava léčiv, cizí jazyk, dále lékárenství nebo farmakognózie.</li>
        </ul>
        <p><strong>Další informace: </strong>školné 1520,- Kč/měsíc denní studium (školné se platí ve dvou pololetních splátách - předem);&nbsp; 16000,-Kč ročně - kombinované studium</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-24">
        <p>Škola <strong>FARMEKO</strong> zahájila svou činnost v roce 1992 a od tohoto roku je zařazena do sítě škol MŠMT ČR - splňuje veškerá kritéria pro kvalitní výuku připravující žáky k&nbsp;maturitě a pro vyšší odborné vzdělávání.</p>
        <p>Za dobu uplynulých let prošly školou stovky absolventů se zaměřením na farmacii, ekologii či laboratorní činnost. Valná většina z nich se velmi dobře uplatnila v praxi či při studiu na vysokých školách. Škola se neustále modernizuje a přizpůsobuje požadavkům legislativy, trhu práce a moderním trendům, aby zajistila absolventům vysokou konkurenceschopnost.</p>
        <p>Budova školy FARMEKO – Vyšší odborné školy zdravotnické a Střední odborné školy, s.r.o. (dále jen FARMEKO) se nachází v&nbsp;jižní části města Jihlavy nedaleko centra v&nbsp;lokalitě zvané Na Slunci. Ze středu města je snadno dostupná pro pěší (cca 10-15 minut chůze) nebo hromadnou městskou dopravou (cca 5 minut jízdy trolejbusem - linka A, E nebo autobusem č. 5).</p>
        <p>Škola FARMEKO se vyznačuje dobrými mezilidskými vztahy, příjemným „rodinným“ prostředím, moderním vybavením, individuálním, partnerským a komunikativním přístupem k&nbsp;žákům a studentům. Snažíme se být školou užitečnou, inspirativní a přátelskou.</p>

        <h3>V&nbsp;současné době škola zajišťuje <strong>vzdělávání v&nbsp; těchto oborech</strong>:</h3>

        <h4>SOŠ:</h4>
        <ul>
            <li>16-01-M/01 <a title="Ekologie a životní prostředí" href="http://www.farmeko.cz/ekologie/" target="_self">Ekologie a životní prostředí</a></li>
            <li>53-43-M/01 <a title="lab. asistent" href="http://www.farmeko.cz/laborant/" target="_self">Laboratorní asistent</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </li>
        </ul>

        <h4>VOŠ:</h4>
        <ul>
            <li>53-43-N/11 &nbsp;&nbsp; <a title="Diplomovaný farmaceutický asistent" href="http://www.farmeko.cz/farmacie/" target="_self">Diplomovaný farmaceutický asistent</a> &nbsp;</li>
        </ul>

        <h4>Kurzy:</h4>
        <ul>
            <li>rekvalifikační kurz kosmetiky „<a title="Kosmetické služby" href="http://www.farmeko.cz/kosmetika/" target="_self">Kosmetické služby</a>“</li>
        </ul>

        <h3>Koncepce a cíle školy</h3>
        <p>Cílem naší školy je poskytnou absolventům nejen odborné vědomosti, dovednosti a návyky, ale také širší všeobecné vzdělání a dovednosti důležité pro občanský život, další vzdělávání a uplatnění se na trhu práce. Konkrétním cílem naší práce je dobře připravit střední odborné pracovníky v environmentální oblasti, tzn. pracovníky pro práci v oblasti ekologie a ochrany životního prostředí a dále střední odborné pracovníky v oblasti diagnostické zdravotní péče a lékárenské zdravotní péče, tj. laboratorní asistenty a diplomované farmaceutické asistenty, a to v souladu s našimi vzdělávacími programy.</p>
        <p><strong>Žáky a studenty učíme poznávat , chránit a vážit si nejcennějších hodnot - lidského zdraví a zdravého životního prostředí.</strong></p>
        <p><strong><em>"Našim cílem je být školou užitečnou, inspirativní a přátelskou".</em></strong></p>

        <h4>K silným stránkám naší školy patří:</h4>
        <ul>
            <li>výhody malé školy, příznivé klima</li>
            <li>velmi dobrá spolupráce se sociálními partnery (organizace a firmy podílející se na praktickém vzdělávání)</li>
            <li>velmi dobré vybavení laboratorní technikou</li>
            <li>velmi dobré vybavení ICT (internet a data projektory v každé učebně, Wi-Fi síť, přenosné PC)</li>
            <li>stabilní pedagogický sbor</li>
        </ul>


        <h3>Spolupráce školy s&nbsp;odbornou praxí, sociální partneři školy</h3>

        <p>Škola FARMEKO je členem profesních sdružení: Sdružení soukromých škol Čech, Moravy a Slezska, Asociace Vyšších odborných škol a Asociace ředitelů zdravotnických škol.</p>
        <p><img title="Sdružení soukromých škol čech, moravy a slezska" alt="Sdružení soukromých škol čech, moravy a slezska" src="http://farmeko.cz/uploaded/clanky/obrazky_do_clanku/partner_01.jpg" rel="" vspace="0" align="" hspace="-1"><img title="Asociace vyšších odborných škol" alt="Asociace vyšších odborných škol" src="http://farmeko.cz/uploaded/clanky/obrazky_do_clanku/partner_02.jpg" rel="" vspace="0" align="" hspace="-1"><img title="Asociace ředitelů zdravotnických škol" alt="Asociace ředitelů zdravotnických škol" src="http://farmeko.cz/uploaded/clanky/obrazky_do_clanku/partner_03.jpg" rel="" vspace="0" align="" hspace="-1"></p>
        <p>Za dobu téměř dvacetileté existence školy se podařilo vybudovat velmi dobré a funkční vztahy s&nbsp;okolním sociálním a pracovním prostředím.&nbsp;&nbsp; V&nbsp;rámci realizace svých vzdělávacích programů škola FARMEKO spolupracuje s&nbsp;celou řadou významných organizací. Těžiště spolupráce spočívá zejména v&nbsp;zajištění praktického vzdělávání, čímž je kvalitně zabezpečena provázanost školního vzdělávacího procesu s&nbsp;praxí.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
        <p>Ke stěžejním <strong>sociálním partnerům</strong> školy FARMEKO patří: Nemocnice Jihlava a ostatní nemocnice v Kraji Vysočina, desítky lékáren po celé ČR a na Slovensku, Česká lékárenská holding, a.s. (společnost provozující síť lékáren Dr. Max), Krajská hygienická stanice kraje Vysočina se sídlem v Jihlavě, Zdravotní ústav se sídlem v&nbsp;Jihlavě, Výzkumný ústav veterinární Brno, Muzeum Vysočiny Jihlava, CHKO Žďárské vrchy, CHKO Třeboňsko, Lesy České republiky, ZOO Jihlava, Ekoinfocentrum Jihlava a mnoho dalších organizací.</p>
        <p>Významným prvkem pro zabezpečení kvality a aktuálnosti teoretické a praktické výuky je pro školu FARMEKO <strong>spolupráce s&nbsp;vysokými školami</strong>, konkrétně&nbsp; s Veterinární a farmaceutickou univerzitou v&nbsp;Brně a Mendlovou zemědělskou a lesnickou univerzitou v&nbsp;Brně. S&nbsp;těmito vysokými školami spolupracujeme při zabezpečování speciálních odborných přednášek, tvorbě a aktualizaci vzdělávacích programů, zajišťování odborných exkurzí a při vedení a realizaci studentských projektů a závěrečných maturitních či absolventských prací.</p>

        <h3>Materiální vybavení</h3>
        <ul>
            <li><strong>Teoretická výuka</strong> probíhá v&nbsp;kmenových učebnách plně vybavených moderním sedacím nábytkem, bílými tabulemi, televizory, DVD přehrávači, videorekordéry a dataprojektory s&nbsp;připojením na PC. K&nbsp;přednáškám a&nbsp;realizaci projektových dnů či kulturním akcím slouží přednáškový areál, jehož součástí je studovna s&nbsp;počítači připojenými k&nbsp;internetu, knihovna, šatna a moderní sociální zařízení.</li>
            <li><strong>Praktická výuka</strong> se realizuje v&nbsp;pěti moderních laboratořích vybavených odpovídajícím laboratorním nábytkem, moderní přístrojovou technikou, laboratorním sklem, chemikáliemi, potřebnými surovinami&nbsp; a dalším zařízením k&nbsp;výuce, včetně moderní přístrojové techniky.</li>
            <li><strong>Škola disponuje</strong> rovněž automatickou meteorologickou stanicí propojenou s&nbsp;počítačem.</li>
            <li><strong>Informační a komunikační technologie</strong> se vyučují v&nbsp;moderní specializované učebně. V&nbsp;počítačích je k&nbsp;dispozici úplné softwarové vybavení. Připojení k&nbsp;internetu a k&nbsp;lokální síti je samozřejmostí. Počítače a připojení k&nbsp;internetu jsou přístupné studentům k&nbsp;samostatné práci, vyhledávání informací a k odborné činnosti. Studenti mohou využívat tiskárny, kopírovací stroje a vázací zařízení.</li>
            <li><strong>Škola je dobře vybavena </strong>učebními pomůckami, učebnicemi, učebními texty a další literaturou. Má svou samostatnou knihovnu, kde si žáci zapůjčují veškerou potřebnou vzdělávací i doplňkovou odbornou literaturu.</li>
        </ul>
    </div>
</div>
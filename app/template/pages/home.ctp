<?php
    $params = array(
        'items' => array(
            array('image'=>'http://farmeko.cz/css_farmeko/images/img4.jpg', 'title'=>'Image 4','description'=>'Zdraví a zdravé životní prostředí je to nejcennější co máme. Učíme se tyto hodnoty chránit, obnovovat a vážit si jich ...'),
            array('image'=>'http://farmeko.cz/css_farmeko/images/img3.jpg', 'title'=>'Image 3','description'=>'Zdraví a zdravé životní prostředí je to nejcennější co máme. Učíme se tyto hodnoty chránit, obnovovat a vážit si jich ...'),
            array('image'=>'http://farmeko.cz/css_farmeko/images/img2.jpg', 'title'=>'Image 2','description'=>'Zdraví a zdravé životní prostředí je to nejcennější co máme. Učíme se tyto hodnoty chránit, obnovovat a vážit si jich ...'),
            array('image'=>'http://farmeko.cz/css_farmeko/images/img1.jpg', 'title'=>'Image 1','description'=>'Zdraví a zdravé životní prostředí je to nejcennější co máme. Učíme se tyto hodnoty chránit, obnovovat a vážit si jich ...')
        ),
        'news' => array(
            array('title'=> '2. roč. VOŠ - důležité sdělen','description'=>'Přihlášení k absolventským pracím pro školní rok 2016/17 proběhne elektronickou formou již od 1. června 2016 !!! ...'),
            array('title'=> 'Výpadek služby elektronická třídní','description'=>'Z důvodu výpadku služeb internetového poskytovatele nebude po omezenou dobu možno využívat služeb elektronické třídní knihy. Za způsobené problémy se omlouváme'),
            array('title'=> 'Obor Dipl. farm. asistent = jistota ','description'=>'VOŠ - TERMÍN PŘIJÍMACÍHO ŘÍZENÍ: 16. 6. 2016 v 10.00 hod. denní forma vzdělávání; ve 13.00 hod. kombinovaná forma vzdělávání; (UCHAZEČI OBDRŽÍ PÍSEMNOU PPOZVÁNKU NA ZÁKLADĚ PŘIHLÁŠKY'),
        )
    );
?>

<div class="row">
    <div class="col-sm-14">
        <div class="carousel relative slider-inner">
            <div id="da-slider" class="da-slider">
                <?php foreach($params['items'] as $key => $slide):?>
                    <div class="da-slide" style="background-size: 100%; background-image:url('<?=$slide['image'];?>');">
                        <h2><i><?=$slide['title'];?></i></h2>
                        <p style="top:190px"><?=$slide['description'];?></p>
                    </div>
                <?php endforeach;?>
                <div class="da-arrows">
                    <span class="da-arrows-prev"></span>
                    <span class="da-arrows-next"></span>
                </div>
            </div>
        </div><!--/slider-->
    </div>
    <div class="col-sm-10">
        <div class="news_panel">
            <h2>Aktualne</h2>
            <div class="items">
                <?php foreach($params['news'] as $key => $new):?>
                <h3><a href="#"><?= $new['title'];?></a></h3>
                <p><?= $new['description'];?></p>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-24">
        <h1>FARMEKO - Vyšší odborná škola zdravotnická a Střední odborná škola</h1>
        <p><strong>Škola FARMEKO</strong> - Vyšší odborná škola zdravotnická a Střední odborná škola zahájila svou činnost v roce 1992. Za dobu uplynulých let prošly školou  stovky absolventů se zaměřením na oblast farmacie, ekologie, laboratorní činnosti či kosmetiky. Valná většina z nich se velmi dobře uplatnila v praxi či při dalším studiu na  vysokých školách a vyšších odborných školách. Škola FARMEKO se neustále modernizuje a přizpůsobuje požadavkům legislativy, trhu práce a moderním trendům tak, aby zajistila absolventům  vysokou konkurenceschopnost.</p>
        <p><strong>Našimi partnery v oblasti praktického vzdělávání (odborné praxe studentů) je  řada organizací působících v environmentální oblasti, nemocnice a jiná zdravotnická zařízení celého regionu Vysočiny poskytující diagnostickou laboratorní péči, desítky nemocničních a soukromých lékáren po celé České republice a na Slovensku a významné společnosti provozující sítě lékáren po celé ČR.</strong></p>
        <p>K výzmamných partnerům naší školy patří např.: Nemocnice Jihlava - diagnostická oddělení - laboratoře, Nemocnice Havlíčkův Brod - laboratoře, Nemocnice Pelhřimov - laboratoře, AeskuLab laboratoře - CEDELAB Mostiště, Česká lékárna holding provozující síť lékáren Dr. Max, síť lékáren BENU, nemocniční lékárna Jihlava, Magistrát města Jihlavy - odbor životního prostředí, přírodovědné oddělení Muzea Vysočiny, Ekoinfocentrum Jihlava, ZOO Jihlava ad.</p>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <ul class="list-group sidebar-nav-v1 ">
            <li class="list-group-item"><a href="javascript:;"><i class="fa fa-arrow-right" aria-hidden="true"></i> O škole</a></li>
            <li class="list-group-item"><a href="javascript:;"><i class="fa fa-arrow-right"></i> Fotogalerie školy</a></li>
            <li class="list-group-item"><a href="javascript:;"><i class="fa fa-arrow-right"></i> Dokumenty ke stažení</a></li>
            <li class="list-group-item"><a href="javascript:;"><i class="fa fa-arrow-right"></i> Důležité kontakty</a></li>
        </ul>
    </div>
    <div class="col-sm-12 text-center">
        <a href="http://www.netkatalog.cz/firma/61105-farmeko-vyssi-odborna-skola-zdravotnicka-a-stredni-odborna-skola-sro/" target="_blank" title="FARMEKO - Vyšší odborná škola zdravotnická a Střední odborná škola, s.r.o. - netkatalog.cz">
            <img src="http://files.netorg.cz/stamp/npi/np130-blue-p1.png" alt="FARMEKO - vyšší odborná škola zdravotnická a střední odborná škola, s.r.o. - netkatalog.cz" />
        </a>
    </div>
</div>

<!DOCTYPE html>
<html>
<head>
    <title>Farmeko</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="assets/css/style.css" rel="stylesheet" media="screen">
    <link href="assets/css/font-awesome.css" rel="stylesheet" media="screen">



</head>
<body>
    <header class="container">
        <div class="col-sm-6 col-xs-24 vcenter">
            <div id="logo" class="hidden-xs">
                <img src="/assets/imgs/logo.png" class="img-responsive m-t-5" alt="logo"/>
            </div>
            <div class="row hidden-sm hidden-lg hidden-md">
                <div class="col-xs-12"><img src="/assets/imgs/logo.png" class="img-responsive m-t-10" alt="logo"/></div>
                <div class="col-xs-12"><img src="/assets/imgs/header.png" class="img-responsive pull-right m-t-15 " alt="Jiz od roku 1992"/></div>
            </div>
        </div>
        <div class="col-sm-12 col-xs-24 vcenter text-center">
            <div class="title">VYŠŠÍ ODBORNÁ ŠKOLA ZDRAVOTNICKÁ A STŘEDNÍ ODBORNÁ ŠKOLA, S.R.O.</div>

        </div>
        <div class="col-sm-5 vcenter">
            <div id="logo2" class="pull-right">
                <img src="/assets/imgs/header.png" class="img-responsive hidden-xs" alt="Jiz od roku 1992"/>
            </div>
        </div>


    </header>
    <section class="container greyBg">
        <div class="row">
            <div class="col-sm-3">
                <ul class="left-menu">
                    <li class="c1"><a href="javascript:;">Farmacie</a></li>
                    <li class="c2"><a href="javascript:;">Ekologie</a></li>
                    <li class="c3"><a href="javascript:;">Laborant</a></li>
                    <li class="c4"><a href="javascript:;">Kosmetika</a></li>
                </ul>

                <ul class="left-menu">
                    <li class="c5"><a href="javascript:;">e-Klasifikace</a></li>
                    <li class="c6"><a href="javascript:;">e-Tridni kniha</a></li>
                </ul>
            </div>

            <div class="col-sm-21 whiteBg ">

                <nav class="navbar navbar-default">
                    <div class="container-fluid padding-0">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
<!--                            <a class="navbar-brand" href="#">Brand</a>-->
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse padding-0" >
                            <ul class="nav navbar-nav">
                                <li class="c0  hidden-sm"><a title="Home" href="index.php?p=/"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                                <li class="dropdown c1">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">O škole <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a title="O škole" href="index.php?p=/o-skole/o-skole/">O škole</a></li>
                                        <li><a title="Diplomovaný farmaceutický asistent" href="index.php?p=/farmacie/">Diplomovaný farmaceutický asistent</a></li>
                                        <li><a title="Ekologie a životní prostředí" href="index.php?p=/ekologie/">Ekologie a životní prostředí</a></li>
                                        <li><a title="Laboratorní asistent" href="index.php?p=/laborant/">Laboratorní asistent</a></li>
                                        <li><a title="Kosmetika" href="index.php?p=/kosmetika/">Kosmetika</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown c2">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pro uchazeče <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a title="Přijímací řízení - SOŠ" href="index.php?p=/pro-uchazece/prijimaci-rizeni-sos/">Přijímací řízení - SOŠ</a></li>
                                        <li><a title="Přijímací řízení - VOŠ" href="index.php?p=/pro-uchazece/prijimaci-rizeni-vos/">Přijímací řízení - VOŠ</a></li>
                                        <li><a title="Ubytování studentů" href="index.php?p=/pro-uchazece/ubytovani-studentu/">Ubytování studentů</a></li>
                                        <li><a title="Potvrzení o zdravotní způsobilosti" href="index.php?p=/pro-uchazece/potvrzeni-o-zdravotni-zpusobilosti/">Potvrzení o zdravotní způsobilosti</a></li>
                                        <li><a title="Přihlášky ke studiu" href="index.php?p=/pro-uchazece/prihlasky-ke-studiu/">Přihlášky ke studiu</a></li>
                                        <li><a title="Školní vzdělávací programy (ŠVP)" href="index.php?p=/pro-uchazece/skolni-vzdelavaci-programy-svp/">Školní vzdělávací programy (ŠVP)</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown c3">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fotogalerie <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a title="Z akcí školy" href="http://farmeko.cz/fotogalerie/2">Z akcí školy</a></li>
                                        <li><a title="Škola" href="http://farmeko.cz/fotogalerie/3">Škola</a></li>
                                        <li><a title="Virtuální ukázky praktické výuky" href="index.php?p=/fotogalerie/virtualni-ukazky-prakticke-vyuky/">Virtuální ukázky praktické výuky</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown c4">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Aktivity studentů <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a title="Projekty, aktivity studentů" href="index.php?p=/aktivity-studentu/projekty-aktivity-studentu/">Projekty, aktivity studentů</a></li>
                                        <li><a title="Fotosoutěž" href="index.php?p=/aktivity-studentu/fotosoutez/">Fotosoutěž</a></li>
                                        <li><a title="ELF-studentský časopis" href="index.php?p=/aktivity-studentu/elf-studentsky-casopis/">ELF-studentský časopis</a></li>
                                        <li><a title="Zelená škola" href="index.php?p=/aktivity-studentu/zelena-skola/">Zelená škola</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown c5">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ke stažení <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a title="Výroční zpráva o škole" href="index.php?p=/ke-stazeni/vyrocni-zprava-o-skole/">Výroční zpráva o škole</a></li>
                                        <li><a title="Školní vzdělávací programy (ŠVP)" href="index.php?p=/ke-stazeni/skolni-vzdelavaci-programy-svp/">Školní vzdělávací programy (ŠVP)</a></li>
                                        <li><a title="Učební plány" href="index.php?p=/ke-stazeni/ucebni-plany/">Učební plány</a></li>
                                        <li><a title="Přihlášky ke studiu" href="index.php?p=/ke-stazeni/prihlasky-ke-studiu/">Přihlášky ke studiu</a></li>
                                        <li><a title="Potvrzení o zdravotní způsobilosti" href="index.php?p=/ke-stazeni/potvrzeni-o-zdravotni-zpusobilosti/">Potvrzení o zdravotní způsobilosti</a></li>
                                        <li><a title="Školní řády " href="index.php?p=/ke-stazeni/skolni-rady/">Školní řády </a></li>
                                    </ul>
                                </li>
                                <li class="c6"><a title="Kontakt" href="index.php?p=/kontakt/">Kontakt</a></li>
                                <li class="c7 hidden-sm"><a title="Přihlášení uživatele" href="index.php?p=/login/"><i class="fa fa-users" aria-hidden="true"></i></a></li>
                                <li class="c8 hidden-sm"><a title="Facebook" href="https://www.facebook.com/pages/Farmeko-VO%C5%A0Z-a-SO%C5%A0-sro-Jihlava/269269753242282?fref=ts"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                
                            </ul>

                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>

                <?php include_once('app/template/pages/'.$page.'.ctp');?>

            </div>
        </div>
    </section>


    <footer class="container m-t-30">
        <div class="row p-t-10">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul>
                    <li>
                        <a href="#">Diplomovaný farmaceutický asistent</a>
                        <ul class="p-t-10">
                            <li class="c1">Tříleté pomaturitní studium zakončené absolutoriem.</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul>
                    <li>
                        <a href="#">Ekologie a životní prostředí</a>
                        <ul class="p-t-10">
                            <li class="c2">Čtyřletý <br/>obor s maturitou.</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul>
                    <li>
                        <a href="#">Laboratorní asistent</a>
                        <ul class="p-t-10">
                            <li class="c3">Čtyřletý <br/>obor s maturitou.</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul>
                    <li>
                        <a href="#">Kosmetické služby</a>
                        <ul class="p-t-10">
                            <li class="c4">Atraktivní 5 měsíční kurz <br/>zakončený zkouškou.</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 hidden-md hidden-sm hidden-xs">
                <img src="assets/imgs/loga.png" title="loga" alt="loga" class="img-responsive"/>
            </div>
        </div>
        <div class="row p-b-10 p-t-10">
            <div class="col-xs-16">© 2011 FARMEKO - Vyšší odborná škola zdravotnická a Střední odborná škola s.r.o.</div>
            <div class="col-xs-8 text-right"><a title="Tisk" onclick="tisk();" href="#">Tisk</a> | <a class="ajax_href" title="Mapa stránek" href="/mapa-stranek/">Mapa stránek</a> | <a title="Nahoru" class="scroll_moo" href="#top">Nahoru</a></div>
        </div>
    </footer>




<!--<h1>Hello, world!</h1>-->
<!--<script src="http://code.jquery.com/jquery.js"></script>-->
<!--<script src="js/bootstrap.min.js"></script>-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>